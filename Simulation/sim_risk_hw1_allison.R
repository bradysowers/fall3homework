# SIMULATION HW1


#TODO
#subset years 1990-2006
#simulate change in cost
  #already have arithmetic changes- should we use pcnt change?
  # use average costs of oil/gas/dry_well (48 obs)
#info on simulation
  #2006-2012 changes consistent- uniform dist
  #2012-2015 decrease by 9.17% per year with max=22% and min=7%
  #2015-2019 increase by 5% per year with max=6% and min=2%
#test to see if 2006-2012 follow normal dist with qq-plots
#build kernel density estimate of distribution of 06-12 using info above
#simulate under assumption of normality and using kernel density estimate

library(dplyr)
library(graphics)
library(ks)
library(readxl)


file.dir <-"C:\\Users\\Allison\\Documents\\Simulation and Risk\\HW 1\\"
price_proj <- read_xlsx("C:\\Users\\Allison\\Documents\\Simulation and Risk\\HW 1\\Analysis_Data.xlsx",1,skip=2)#,col_types="numeric")
drill_cost <- read_xlsx("C:\\Users\\Allison\\Documents\\Simulation and Risk\\HW 1\\Analysis_Data.xlsx",2,skip=2,col_types = c("guess","numeric","numeric","numeric","numeric","numeric","numeric"))


#subsetting 1990-2006
drill_cost <- drill_cost[31:47,]

#average
drill_cost <- mutate(drill_cost, avg_return=(drill_cost$`Arithmetic Return - Crude Oil`+
                        drill_cost$`Arithmetic Return - Natural Gas`+
                        drill_cost$`Arithmetic Return - Dry Well`)/3)

#kde
cost_density <- density(drill_cost$avg_return, bw="SJ-ste") #gives bw=0.04457

#randomly sample from kernel density estimate(kde)
Est.return <- rkde(fhat=kde(drill_cost$avg_return, h=0.04457), n=100000)
hist(Est.return, breaks=50)

