for (i in 1:sim){
print("Begin")
uniform = runif(n=1, min = 10, max = 30) # simulate the number of wells using the uniform distribution
unif_int = round(uniform, 0) # change uniform to an integer in order to simulate how many wells are wet for each
hc = rtruncnorm(n = unif_int, a = 0, b = 1, mean = 0.99, sd = 0.05) # simulate hydrocarbon present probability
res = rtruncnorm(n = unif_int, a = 0, b = 1, mean = 0.80, sd = 0.10) # simulate reservoir present probability
well = hc * res # calculate total probability of a producing well
bernoulli = rbinom(n=unif_int, size = 1, prob = well) # bernoulli distribution to simulate how many of the wells are wet or dry
num_wet = sum(bernoulli)
num_dry = unif_int-sum(bernoulli)
print("Count Done")
################# Production Risk and Revenue Risk
####################################
standardize <- function(x){
x.std = (x - mean(x))/sd(x)
return(x.std)
}
destandardize <- function(x.std, x){
x.old = (x.std * sd(x)) + mean(x)
return(x.old)
}
##Simulated costs for 2019##
for (j in 2:7) {
oil[j] = sapply(oil[j], as.numeric)
}
oil_change = oil[,5:7]
drill_cost = gather(oil_change, "Return_type", "Return_amount", 1:3)
oil$avg_cost = rowMeans(oil[2:4])
mean_norm = mean(drill_cost$Return_amount, na.rm=TRUE)
sd_norm = sd(drill_cost$Return_amount, na.rm=TRUE)
##Calculate cost for the Dry Wells
print("Start Dry Well")
if (num_dry > 0) {
lease_r = rnorm(num_dry, 600,50)
lease_s = lease_r*960
# distribution for seismic sections per well
seis_r = rnorm(num_dry,3,.35)
seis_s = seis_r*43000
#distributions for completion costs
comp_r = rnorm(num_dry,390000,50000)
#professional overhead distribution
overhead_r = rtriangle(n = num_dry,a = 172000,b = 279500,c =215000)
p0 = as.numeric(oil$avg_cost[16])
#Initializes a vector of our average 2006 price for the size of the simulation
p13 = rep(p0, num_dry)
#We first simulate the cost for 2007 by building a normal distribution over the percent changes for 1991-2006
r = rnorm(n=num_dry, mean = mean_norm, sd=sd_norm)
change = p0*r
p13 = p13 + change
#This is simulating the costs for 2008-2012
for(j in 1:5){
r = rnorm(n=num_dry, mean = mean_norm, sd=sd_norm)
change = p13*r
p13 = p13 + change
}
#This is simulating the costs for 2013-2015
for (j in 1:3){
r = rtriangle(n=num_dry, a=-.22, b=-.07, c=-.0917)
change = p13*r
p13 = p13 + change
}
#This is simulating the costs for 2016-2019
for (j in 1:4){
r = rtriangle(n=num_dry, a=.02, b=.06, c=.05)
change = p13*r
pt = p13 + change
}
p13 = p13*1000
#The total cost of a simulation's dry wells
dry_well_cost = sum(seis_s + lease_s + overhead_r + p13)
} else {
dry_well_cost = 0
}
print("End Dry Well")
##Calculate cost for the Wet Wells
print("Start Wet Well")
lease_r = rnorm(num_wet, 600,50)
lease_s = lease_r*960
na.omit(lease_s)
# distribution for seismic sections per well
seis_r = rnorm(num_wet,3,.35)
seis_s = seis_r*43000
#distributions for completion costs
comp_r = rnorm(num_wet,390000,50000)
#professional overhead distribution
overhead_r = rtriangle(n = num_wet,a = 172000,b = 279500,c =215000)
p0 = as.numeric(oil$avg_cost[16])
#Initializes a vector of our average 2006 price for the size of the simulation
p13 = rep(p0, num_wet)
#We first simulate the cost for 2007 by building a normal distribution over the percent changes for 1991-2006
r = rnorm(n=num_wet, mean = mean_norm, sd=sd_norm)
change = p0*r
p13 = p13 + change
#This is simulating the costs for 2008-2012
for(j in 1:5){
r = rnorm(n=num_wet, mean = mean_norm, sd=sd_norm)
change = p13*r
p13 = p13 + change
}
#This is simulating the costs for 2013-2015
for (j in 1:3){
r = rtriangle(n=num_wet, a=-.22, b=-.07, c=-.0917)
change = p13*r
p13 = p13 + change
}
#This is simulating the costs for 2016-2019
for (j in 1:4){
r = rtriangle(n=num_wet, a=.02, b=.06, c=.05)
change = p13*r
pt = p13 + change
}
p13 = p13*1000
Init = seis_s + lease_s + comp_r + overhead_r  + p13
NPV = -Init
print("End Initial Cost Wet")
RateB = rlnorm(num_wet,6,.28) #Begin rate
DR = runif(num_wet,.15,.32) #Decline rate
Corr = matrix(data=cbind(1,.64,.64,1), nrow=2) #imposed a correlation coefficient of 0.64
U = t(chol(Corr)) #cholesky factorization (finds U in LU decomposition)
Both.r = cbind(standardize(DR), standardize(RateB)) #makes standardized
SB.r = U%*%t(Both.r) #matrix multiplication for u with created correlation structure
SB.r = t(SB.r) #transposes matrix
final.r = cbind(destandardize(SB.r[,1], DR), destandardize(SB.r[,2],RateB))
RateB = final.r[,2]
for (k in 1:15){ #15 year period
cost =  overhead_r #setting initial cost as end cost from last year
RateE = (rep(1,num_wet)-final.r[,1]) * RateB #Calculates year end rate
Oil = 365*((RateB+RateE)/2) #Yearly production volumes in barrels of oil
RateB = RateE #Reinitialize to begin year rate
OilPrice = rtriangle(num_wet,as.numeric(price_proj[k+2, 3]),as.numeric(price_proj[k+2,2]),as.numeric(price_proj[k+2,4])) #Calculates oil price
NRI = rnorm(num_wet,.75,.02) #Calculates net revenue interest
Rev = (Oil*OilPrice)*NRI #Annual revenues before the royalty payments
Operating = rnorm(num_wet,2.25,.30) #Operating expenses
OperCost = Operating*Oil #Operating costs
Sev = Rev*(1-.046) #Severance taxes
FNR = Sev - OperCost - cost #Final net revenues during specified year
NPV = NPV + (FNR/(1.1^k)) #Net present value
}
print("End")
total[i] = sum(sum(NPV), -sum(dry_well_cost))
}
hist(total)
total
#########################
#Simulation Final Report
#########################
path = "C:/Users/Chris/Desktop/School/Fall3/SimulationRisk/Simulation_Analysis_Data.xlsx"
library(readxl)
library(graphics)
library(ks)
library(triangle)
library(tidyverse)
library(lubridate)
library(scales)
library(truncnorm)
set.seed(12345)
price_proj <- read_excel(path, sheet=1)
drill_cost <- read_excel(path, sheet=2)
oil = drill_cost[34:49,]
sim = 10000
total = rep(0,sim)
for (i in 1:sim){
print("Begin")
uniform = runif(n=1, min = 10, max = 30) # simulate the number of wells using the uniform distribution
unif_int = round(uniform, 0) # change uniform to an integer in order to simulate how many wells are wet for each
hc = rtruncnorm(n = unif_int, a = 0, b = 1, mean = 0.99, sd = 0.05) # simulate hydrocarbon present probability
res = rtruncnorm(n = unif_int, a = 0, b = 1, mean = 0.80, sd = 0.10) # simulate reservoir present probability
well = hc * res # calculate total probability of a producing well
bernoulli = rbinom(n=unif_int, size = 1, prob = well) # bernoulli distribution to simulate how many of the wells are wet or dry
num_wet = sum(bernoulli)
num_dry = unif_int-sum(bernoulli)
print("Count Done")
################# Production Risk and Revenue Risk
####################################
standardize <- function(x){
x.std = (x - mean(x))/sd(x)
return(x.std)
}
destandardize <- function(x.std, x){
x.old = (x.std * sd(x)) + mean(x)
return(x.old)
}
##Simulated costs for 2019##
for (j in 2:7) {
oil[j] = sapply(oil[j], as.numeric)
}
oil_change = oil[,5:7]
drill_cost = gather(oil_change, "Return_type", "Return_amount", 1:3)
oil$avg_cost = rowMeans(oil[2:4])
mean_norm = mean(drill_cost$Return_amount, na.rm=TRUE)
sd_norm = sd(drill_cost$Return_amount, na.rm=TRUE)
##Calculate cost for the Dry Wells
print("Start Dry Well")
if (num_dry > 0) {
lease_r = rnorm(num_dry, 600,50)
lease_s = lease_r*960
# distribution for seismic sections per well
seis_r = rnorm(num_dry,3,.35)
seis_s = seis_r*43000
#distributions for completion costs
comp_r = rnorm(num_dry,390000,50000)
#professional overhead distribution
overhead_r = rtriangle(n = num_dry,a = 172000,b = 279500,c =215000)
p0 = as.numeric(oil$avg_cost[16])
#Initializes a vector of our average 2006 price for the size of the simulation
p13 = rep(p0, num_dry)
#We first simulate the cost for 2007 by building a normal distribution over the percent changes for 1991-2006
r = rnorm(n=num_dry, mean = mean_norm, sd=sd_norm)
change = p0*r
p13 = p13 + change
#This is simulating the costs for 2008-2012
for(j in 1:5){
r = rnorm(n=num_dry, mean = mean_norm, sd=sd_norm)
change = p13*r
p13 = p13 + change
}
#This is simulating the costs for 2013-2015
for (j in 1:3){
r = rtriangle(n=num_dry, a=-.22, b=-.07, c=-.0917)
change = p13*r
p13 = p13 + change
}
#This is simulating the costs for 2016-2019
for (j in 1:4){
r = rtriangle(n=num_dry, a=.02, b=.06, c=.05)
change = p13*r
pt = p13 + change
}
p13 = p13*1000
#The total cost of a simulation's dry wells
dry_well_cost = sum(seis_s + lease_s + overhead_r + p13)
} else {
dry_well_cost = 0
}
print("End Dry Well")
##Calculate cost for the Wet Wells
print("Start Wet Well")
lease_r = rnorm(num_wet, 600,50)
lease_s = lease_r*960
na.omit(lease_s)
# distribution for seismic sections per well
seis_r = rnorm(num_wet,3,.35)
seis_s = seis_r*43000
#distributions for completion costs
comp_r = rnorm(num_wet,390000,50000)
#professional overhead distribution
overhead_r = rtriangle(n = num_wet,a = 172000,b = 279500,c =215000)
p0 = as.numeric(oil$avg_cost[16])
#Initializes a vector of our average 2006 price for the size of the simulation
p13 = rep(p0, num_wet)
#We first simulate the cost for 2007 by building a normal distribution over the percent changes for 1991-2006
r = rnorm(n=num_wet, mean = mean_norm, sd=sd_norm)
change = p0*r
p13 = p13 + change
#This is simulating the costs for 2008-2012
for(j in 1:5){
r = rnorm(n=num_wet, mean = mean_norm, sd=sd_norm)
change = p13*r
p13 = p13 + change
}
#This is simulating the costs for 2013-2015
for (j in 1:3){
r = rtriangle(n=num_wet, a=-.22, b=-.07, c=-.0917)
change = p13*r
p13 = p13 + change
}
#This is simulating the costs for 2016-2019
for (j in 1:4){
r = rtriangle(n=num_wet, a=.02, b=.06, c=.05)
change = p13*r
pt = p13 + change
}
p13 = p13*1000
Init = seis_s + lease_s + comp_r + overhead_r  + p13
NPV = -Init
print("End Initial Cost Wet")
RateB = rlnorm(num_wet,6,.28) #Begin rate
DR = runif(num_wet,.15,.32) #Decline rate
Corr = matrix(data=cbind(1,.64,.64,1), nrow=2) #imposed a correlation coefficient of 0.64
U = t(chol(Corr)) #cholesky factorization (finds U in LU decomposition)
Both.r = cbind(standardize(DR), standardize(RateB)) #makes standardized
SB.r = U%*%t(Both.r) #matrix multiplication for u with created correlation structure
SB.r = t(SB.r) #transposes matrix
final.r = cbind(destandardize(SB.r[,1], DR), destandardize(SB.r[,2],RateB))
RateB = final.r[,2]
for (k in 1:15){ #15 year period
cost =  overhead_r #setting initial cost as end cost from last year
RateE = (rep(1,num_wet)-final.r[,1]) * RateB #Calculates year end rate
Oil = 365*((RateB+RateE)/2) #Yearly production volumes in barrels of oil
RateB = RateE #Reinitialize to begin year rate
OilPrice = rtriangle(num_wet,as.numeric(price_proj[k+2, 3]),as.numeric(price_proj[k+2,2]),as.numeric(price_proj[k+2,4])) #Calculates oil price
NRI = rnorm(num_wet,.75,.02) #Calculates net revenue interest
Rev = (Oil*OilPrice)*NRI #Annual revenues before the royalty payments
Operating = rnorm(num_wet,2.25,.30) #Operating expenses
OperCost = Operating*Oil #Operating costs
Sev = Rev*(1-.046) #Severance taxes
FNR = Sev - OperCost - cost #Final net revenues during specified year
NPV = NPV + (FNR/(1.1^k)) #Net present value
}
print("End")
total[i] = sum(sum(NPV), -sum(dry_well_cost))
}
hist(total)
min(total)
max(total)
#########################
#Simulation Final Report
#########################
path = "C:/Users/Chris/Desktop/School/Fall3/SimulationRisk/Simulation_Analysis_Data.xlsx"
library(readxl)
library(graphics)
library(ks)
library(triangle)
library(tidyverse)
library(lubridate)
library(scales)
library(truncnorm)
set.seed(65241)
price_proj <- read_excel(path, sheet=1)
drill_cost <- read_excel(path, sheet=2)
oil = drill_cost[34:49,]
sim = 10000000
total = rep(0,sim)
for (i in 1:sim){
uniform = runif(n=1, min = 10, max = 30) # simulate the number of wells using the uniform distribution
unif_int = round(uniform, 0) # change uniform to an integer in order to simulate how many wells are wet for each
hc = rtruncnorm(n = unif_int, a = 0, b = 1, mean = 0.99, sd = 0.05) # simulate hydrocarbon present probability
res = rtruncnorm(n = unif_int, a = 0, b = 1, mean = 0.80, sd = 0.10) # simulate reservoir present probability
well = hc * res # calculate total probability of a producing well
bernoulli = rbinom(n=unif_int, size = 1, prob = well) # bernoulli distribution to simulate how many of the wells are wet or dry
num_wet = sum(bernoulli)
num_dry = unif_int-num_wet
################# Production Risk and Revenue Risk
####################################
standardize <- function(x){
x.std = (x - mean(x))/sd(x)
return(x.std)
}
destandardize <- function(x.std, x){
x.old = (x.std * sd(x)) + mean(x)
return(x.old)
}
##Simulated costs for 2019##
for (j in 2:7) {
oil[j] = sapply(oil[j], as.numeric)
}
oil_change = oil[,5:7]
drill_cost = gather(oil_change, "Return_type", "Return_amount", 1:3)
oil$avg_cost = rowMeans(oil[2:4])
mean_norm = mean(drill_cost$Return_amount, na.rm=TRUE)
sd_norm = sd(drill_cost$Return_amount, na.rm=TRUE)
##Calculate cost for the Dry Wells
if (num_dry > 0) {
lease_r = rnorm(num_dry, 600,50)
lease_s = lease_r*960
# distribution for seismic sections per well
seis_r = rnorm(num_dry,3,.35)
seis_s = seis_r*43000
#distributions for completion costs
comp_r = rnorm(num_dry,390000,50000)
#professional overhead distribution
overhead_r = rtriangle(n = num_dry,a = 172000,b = 279500,c =215000)
p0 = as.numeric(oil$avg_cost[16])
#Initializes a vector of our average 2006 price for the size of the simulation
p13 = rep(p0, num_dry)
#We first simulate the cost for 2007 by building a normal distribution over the percent changes for 1991-2006
r = rnorm(n=num_dry, mean = mean_norm, sd=sd_norm)
change = p0*r
p13 = p13 + change
#This is simulating the costs for 2008-2012
for(j in 1:5){
r = rnorm(n=num_dry, mean = mean_norm, sd=sd_norm)
change = p13*r
p13 = p13 + change
}
#This is simulating the costs for 2013-2015
for (j in 1:3){
r = rtriangle(n=num_dry, a=-.22, b=-.07, c=-.0917)
change = p13*r
p13 = p13 + change
}
#This is simulating the costs for 2016-2019
for (j in 1:4){
r = rtriangle(n=num_dry, a=.02, b=.06, c=.05)
change = p13*r
pt = p13 + change
}
p13 = p13*1000
#The total cost of a simulation's dry wells
dry_well_cost = seis_s + lease_s + overhead_r + p13
} else {
dry_well_cost = 0
}
##Calculate cost for the Wet Wells
if (num_wet > 0) {
lease_r = rnorm(num_wet, 600,50)
lease_s = lease_r*960
# distribution for seismic sections per well
seis_r = rnorm(num_wet,3,.35)
seis_s = seis_r*43000
#distributions for completion costs
comp_r = rnorm(num_wet,390000,50000)
#professional overhead distribution
overhead_r = rtriangle(n = num_wet,a = 172000,b = 279500,c =215000)
p0 = as.numeric(oil$avg_cost[16])
#Initializes a vector of our average 2006 price for the size of the simulation
p13 = rep(p0, num_wet)
#We first simulate the cost for 2007 by building a normal distribution over the percent changes for 1991-2006
r = rnorm(n=num_wet, mean = mean_norm, sd=sd_norm)
change = p0*r
p13 = p13 + change
#This is simulating the costs for 2008-2012
for(j in 1:5){
r = rnorm(n=num_wet, mean = mean_norm, sd=sd_norm)
change = p13*r
p13 = p13 + change
}
#This is simulating the costs for 2013-2015
for (j in 1:3){
r = rtriangle(n=num_wet, a=-.22, b=-.07, c=-.0917)
change = p13*r
p13 = p13 + change
}
#This is simulating the costs for 2016-2019
for (j in 1:4){
r = rtriangle(n=num_wet, a=.02, b=.06, c=.05)
change = p13*r
pt = p13 + change
}
p13 = p13*1000
Init = seis_s + lease_s + comp_r + overhead_r  + p13
NPV = -Init
RateB = rlnorm(num_wet,6,.28) #Begin rate
DR = runif(num_wet,.15,.32) #Decline rate
Corr = matrix(data=cbind(1,.64,.64,1), nrow=2) #imposed a correlation coefficient of 0.64
U = t(chol(Corr)) #cholesky factorization (finds U in LU decomposition)
Both.r = cbind(standardize(DR), standardize(RateB)) #makes standardized
SB.r = U%*%t(Both.r) #matrix multiplication for u with created correlation structure
SB.r = t(SB.r) #transposes matrix
final.r = cbind(destandardize(SB.r[,1], DR), destandardize(SB.r[,2],RateB))
RateB = final.r[,2]
for (k in 1:15){ #15 year period
cost =  overhead_r #setting initial cost as end cost from last year
RateE = (rep(1,num_wet)-final.r[,1]) * RateB #Calculates year end rate
Oil = 365*((RateB+RateE)/2) #Yearly production volumes in barrels of oil
RateB = RateE #Reinitialize to begin year rate
OilPrice = rtriangle(1,as.numeric(price_proj[k+2, 3]),as.numeric(price_proj[k+2,2]),as.numeric(price_proj[k+2,4])) #Calculates oil price
NRI = rnorm(num_wet,.75,.02) #Calculates net revenue interest
Rev = (Oil*OilPrice)*NRI #Annual revenues before the royalty payments
Operating = rnorm(1,2.25,.30) #Operating expenses
OperCost = Operating*Oil #Operating costs
Sev = Rev*(1-.046) #Severance taxes
FNR = Sev - OperCost - cost #Final net revenues during specified year
NPV = NPV + (FNR/(1.1^k)) #Net present value
}
} else {
NPV = 0
}
total[i] = sum(sum(NPV), -sum(dry_well_cost))
}
VaR = quantile(total, probs=0.05) # $77,666,535
CVaR = mean(total[total < VaR]) # $60,176,462
save.image("C:/Users/Chris/Desktop/School/Fall3/fall3homework/Simulation/Final10Mil.RData")
hist(total)
count(total)
sum(is.na(total))
VaR = quantile(total, probs=0.05, na.rm=TRUE) # $77,666,535
CVaR = mean(total[total < VaR]) # $60,176,462
VaR
CVaR
CVaR = mean(total[total < VaR], na.rm=TRUE) # $60,176,462
CVaR
test = filter(total, is.na)
save.image("C:/Users/Chris/Desktop/School/Fall3/fall3homework/Simulation/Final10Mil.RData")
min(total)
min(total, na.rm=TRUE)
load("C:/Users/Chris/Desktop/School/Fall3/fall3homework/Simulation/Final10Mil.RData")
max(total)
max(total, na.rm=TRUE)
quantile(total, probs=.01, na.rm=TRUE)
CVaR = mean(total[total < 42584366], na.rm=TRUE)
CVaR
VaR = quantile(total, probs=0.05, na.rm=TRUE) # $70,756,220
CVaR = mean(total[total < VaR], na.rm=TRUE) # $53,554,014
hist(total)
library(readxl)
library(graphics)
library(ks)
library(triangle)
library(tidyverse)
library(lubridate)
library(scales)
library(truncnorm)
library(dplyr)
library(tidyverse)
total_df = as.data.frame(total)
total_df = total_df %>%
filter(!is.na(total)) %>%
mutate(risk = if_else(total < 70360000, TRUE, FALSE)) %>%
arrange(total)
# Plot the distribution of Total
ggplot(total_df, aes(total, fill=risk)) + geom_histogram(col="black", alpha=0.6, bins=77) +
xlab("NPV after 15 Years") + ylab("Proportion of Simulations") +
scale_fill_manual(values = c("blue", "red")) +
geom_vline(xintercept=70756220, col="red", alpha=0.6) +
geom_vline(xintercept = 171806616, col = "black", alpha=0.6) +
ggtitle("NPV after 15 Years") +
scale_y_continuous(labels=function(x)x/10000000) +
scale_x_continuous(labels=dollar_format(prefix="$")) +
annotate("text", x = 10000000, y = 600000, label = "5% VaR: $70,756,220", col="red") +
annotate("text", x = 236000000, y = 600000, label = "Median: $171,806,616", col="black") +
guides(fill=FALSE)
