path = "Simulation_Analysis_Data.xlsx"
library(readxl)
library(graphics)
library(ks)
library(triangle)
library(tidyverse)
library(lubridate)
set.seed(12345)

price_proj <- read_excel(path, sheet=1)
drill_cost <- read_excel(path, sheet=2)

names(price_proj)
summary(price_proj)
# price_proj: these are prices projected each year 2019 to 2050. 

names(drill_cost)
summary(drill_cost)
str(drill_cost)

#########################
#PRICE OF SINGLE DRY WELL#
#########################

sim = 1000000


#distribution for the leased acres per well
lease_r = rnorm(sim, 600,50)
lease_s = lease_r*960
summary(lease_s)
na.omit(lease_s)
hist(lease_s)


# distribution for seismic sections per well

seis_r = rnorm(sim,3,.35)
seis_s = seis_r*43000
summary(seis_s)
hist(seis_s)

#distributions for completion costs
comp_r = rnorm(sim,390000,50000)



#professional overhead distribution
overhead_r = rtriangle(n = sim,a = 172000,b = 279500,c =215000)
summary(overhead_r)
hist(overhead_r)

dry_well = seis_s+overhead_r+lease_s
summary(dry_well)
sd(dry_well)

asdf = rtriangle(sim,926296,52777.41)
hist(asdf)
hist(dry_well)
################# Production Risk and Revenue Risk
####################################
standardize <- function(x){
  x.std = (x - mean(x))/sd(x)
  return(x.std)
}

destandardize <- function(x.std, x){
  x.old = (x.std * sd(x)) + mean(x)
  return(x.old)
}
######################################

# Init = seis_s + lease_s + comp_r + overhead_r  + 3528000 #cost for 2019 (previous report)
# NPV = -Init
# 
# for (i in 1:15){ #15 year period
#   cost =  overhead_r #setting initial cost as end cost from last year
#   if (i == 1){ #only for the first year do this
#     RateB = rlnorm(sim,6,.28) #Begin rate
#     DR = runif(sim,.15,.32) #Decline rate
#     Corr = matrix(data=cbind(1,.64,.64,1), nrow=2) #imposed a correlation coefficient of 0.64
#     U = t(chol(Corr)) #cholesky factorization (finds U in LU decomposition)
#     Both.r = cbind(standardize(DR), standardize(RateB)) #makes standardized
#     SB.r = U%*%t(Both.r) #matrix multiplication for u with created correlation structure
#     SB.r = t(SB.r) #transposes matrix
#     final.r = cbind(destandardize(SB.r[,1], DR), destandardize(SB.r[,2],RateB)) #destandardizes after combining
#   } #end first year conditions
#   else{
#     RateE = (rep(1,sim)-final.r[,1]) * final.r[,2] #Calculates year end rate
#     Oil = 365*((RateB+RateE)/2) #Yearly production volumes in barrels of oil
#     RateB = RateE #Reinitialize to begin year rate
#     OilPrice = rtriangle(sim,30.38,100,56.64) #Calculates oil price
#     NRI = rnorm(sim,.75,.02) #Calculates net revenue interest
#     Rev = Rev + (Oil*OilPrice)*NRI #Annual revenues before the royalty payments
#     Operating = rnorm(sim,2.25,.30) #Operating expenses
#     OperCost = Operating*Oil #Operating costs
#     Sev = Rev*(1-.046) #Severance taxes
#     FNR = Sev - OperCost - cost #Final net revenues during specified year
#     NPV = NPV + (FNR/(1.1^i)) #Net present value
#   }
# }
# hist(NPV)



##Simulated costs for 2019##
oil = drill_cost[34:49,]

for (i in 2:7) {
  oil[i] = sapply(oil[i], as.numeric)
}
oil_change = oil[,5:7]

drill_cost = gather(oil_change, "Return_type", "Return_amount", 1:3)

oil$avg_cost = rowMeans(oil[2:4])

mean_norm = mean(drill_cost$Return_amount, na.rm=TRUE)
sd_norm = sd(drill_cost$Return_amount, na.rm=TRUE)

p0 = as.numeric(oil$avg_cost[16])

#Initializes a vector of our average 2006 price for the size of the simulation
p13 = rep(p0, sim)

#We first simulate the cost for 2007 by building a normal distribution over the percent changes for 1991-2006
r = rnorm(n=sim, mean = mean_norm, sd=sd_norm)
change = p0*r
p13 = p13 + change

#This is simulating the costs for 2008-2012
for(j in 1:5){
  r = rnorm(n=sim, mean = mean_norm, sd=sd_norm)
  change = p13*r
  p13 = p13 + change
}

#This is simulating the costs for 2013-2015
for (j in 1:3){
  r = rtriangle(n=sim, a=-.22, b=-.07, c=-.0917)
  change = p13*r
  p13 = p13 + change
}

#This is simulating the costs for 2016-2019
for (j in 1:4){
  r = rtriangle(n=sim, a=.02, b=.06, c=.05)
  change = p13*r
  pt = p13 + change
}

p13 = p13*1000

############### second try at this: 
Init = seis_s + lease_s + comp_r + overhead_r  + p13 #cost for 2019 (previous report)
dry_well = seis_s + lease_s + overhead_r + p13
NPV = -Init


RateB = rlnorm(sim,6,.28) #Begin rate
DR = runif(sim,.15,.32) #Decline rate
Corr = matrix(data=cbind(1,.64,.64,1), nrow=2) #imposed a correlation coefficient of 0.64
U = t(chol(Corr)) #cholesky factorization (finds U in LU decomposition)
Both.r = cbind(standardize(DR), standardize(RateB)) #makes standardized
SB.r = U%*%t(Both.r) #matrix multiplication for u with created correlation structure
SB.r = t(SB.r) #transposes matrix
final.r = cbind(destandardize(SB.r[,1], DR), destandardize(SB.r[,2],RateB))
RateB = final.r[,2]

for (i in 1:15){ #15 year period
  cost =  overhead_r #setting initial cost as end cost from last year
  RateE = (rep(1,sim)-final.r[,1]) * RateB #Calculates year end rate
  Oil = 365*((RateB+RateE)/2) #Yearly production volumes in barrels of oil
  RateB = RateE #Reinitialize to begin year rate
  OilPrice = rtriangle(sim,as.numeric(price_proj[i+2, 3]),as.numeric(price_proj[i+2,2]),as.numeric(price_proj[i+2,4])) #Calculates oil price
  NRI = rnorm(sim,.75,.02) #Calculates net revenue interest
  Rev = (Oil*OilPrice)*NRI #Annual revenues before the royalty payments
  Operating = rnorm(sim,2.25,.30) #Operating expenses
  OperCost = Operating*Oil #Operating costs
  Sev = Rev*(1-.046) #Severance taxes
  FNR = Sev - OperCost - cost #Final net revenues during specified year
  NPV = NPV + (FNR/(1.1^i)) #Net present value
  print(summary(NPV))
}

hist(NPV)
