####################### ML HW: Ridge Regression ##########################################
library(tidyverse)
library(glmnet)

names(MLProjectData)
names(test.data)
glimpse(MLProjectData)
summary(MLProjectData)

# target variable summary
mean(MLProjectData$target)
sd(MLProjectData$target)
hist(MLProjectData$target)

# set up training and validation sets
set.seed(4519)
train=sample(c(T,F), nrow(MLProjectData), rep=TRUE, p=c(0.7,0.3))
valid=!train


# X is the design matrix, removing the intercept
# y is a vector containing the values of the target variable

X = model.matrix(target~. ,data=MLProjectData)[,-1]
y = MLProjectData$target

# grid of lambdas to test
set.seed(35739)
grid = 10^seq(10,-2,length=100) 

# determine best lambda 
cv.out = cv.glmnet(X[train,], y[train], alpha=0, lambda=grid) 
plot(cv.out)
bestlambda=cv.out$lambda.min
bestlambda # 10.72


#Run validation data
ridge.mod = glmnet(X[valid,], y[valid], alpha=0, lambda=bestlambda)
pred.ridge = predict(ridge.mod, newx=X[valid,])
val.MSE.ridge = mean((pred.ridge - y[valid])^2)
val.MSE.ridge # 2.042

# Calculate MAPE
ridge.mape = abs((y[valid]-pred.ridge)/y[valid])
ridge.mape1 = sum(ridge.mape)/1879
#MAPE is 5%

# Calculate MAE
ridge.mae = abs(y[valid]-pred.ridge)
ridge.mae1 = sum(ridge.mae)/1879
#MAE is 0.96

# MAPE: 
# sum of (abs value of (actual - pred)/actual)
# divided by n

#MAE:
# sum of (abs value of (actual - pred))
# divided by n



# Run on all data
out=glmnet(X,y,alpha=0, lambda=bestlambda)
ridge.coef = predict(out, type="coefficients")
ridge.coef

# most coefficients are fairly small, num4, 5, 6 are the largest (absolute) values


# Add a column called target in order to make predictions
test.data2 = test.data %>%
  mutate(target = 0)

# Predict test.data 
newX = model.matrix(target~. ,data=test.data2)[,-1]
test.pred = predict(out, newx=newX, lambda = bestlambda)

setwd("C:\\Users\\Melissa Sandahl\\OneDrive\\Documents\\School\\MSA courses\\AA502\\Machine Learning\\HW\\")
write_csv(as.data.frame(test.pred), 'HW_RidgeReg_Predictions.csv')
